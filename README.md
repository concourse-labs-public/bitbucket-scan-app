# Concourse Labs Bitbucket Scan Application

https://concourse-labs.readme.io/docs/bitbucket-data-center-and-server

## Release Notes

### 3.0.0
- Added new environment variable `CONCOURSELABS_DNS` supporting use of ConcourseLabs on other available cloud providers. Default setting is AWS.

*Release Date: 2023-04-06*

### 2.0.0
- Added surface layer id support.
- Removed attribute tag id support.

*Release Date: 2022-09-30*


### 1.0.1
- Upgraded to Auth0 authentication.
- Upgraded settings config page to store username and secret.

*Release Date: 2022-09-16*


### 1.0.0
- Evaluation of newly added or edited AWS, Azure, and Terraform files in repository where plugin is configured
- Configuration of target Attribute Tags and Surface Layer to run template evaluations against
- Configuration of a matching branch regex to limit which branches to run the scan against
- Configuration of a Severity level to determine if violations block the event
- CLI output of scan results from clients pushing to repositories or projects with plugin enabled
- Markdown-Formatted output of scan results in the activity comments of a Pull Request

*Release Date: 2022-02-14*